package ru.allfound.anim2GISpr;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

/*
 * MainActivity.java    v.1.0 20.12.2015
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class MainActivity extends Activity implements View.OnClickListener {

    public static final int GPS_TIME_UPDATE = 10000;
    public static final int MIN_DISTANCE = 10;
    static final boolean TEST = false;
    DialogAddData dialogAddData;
    Button addButton;
    Button outDataButton;
    private LocationManager locationManager;
    TextView textViewGPS;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dialogAddData = new DialogAddData(this);
        DialogAddData.listViewAnimals = (ListView) findViewById(R.id.listViewAnimals);
        TextView textViewLinks = (TextView) findViewById(R.id.textViewLinksData);
        registerForContextMenu(DialogAddData.listViewAnimals);
        addButton = (Button) findViewById(R.id.addButtton);
        addButton.setOnClickListener(this);
        outDataButton = (Button) findViewById(R.id.outDataButton);
        outDataButton.setOnClickListener(this);
        textViewGPS = (TextView) findViewById(R.id.textViewGPS);
        AsyncLoadDataFromBD asyncLoadDataFromBD = new AsyncLoadDataFromBD();
        asyncLoadDataFromBD.execute();
        checkGPS();
    }

    private void checkGPS() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)) //if GPS is true
        {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location == null) {
                textViewGPS.setText("GPS: 0.00000, 0.00000");
                textViewGPS.setTextColor(getResources().getColor(R.color.colorGPS));
            } else {
                textViewGPS.setText("GPS: " +   String.format("%.5f", location.getLatitude()) + ", " +
                        String.format("%.5f", location.getLongitude()) );
                textViewGPS.setTextColor(getResources().getColor(R.color.colorGPS));
            }
            //включаем GPS на постоянку пока программа не уничтожится чтобы постоянно обновлялись данные
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_TIME_UPDATE, MIN_DISTANCE, locationListener);
        } else {
            textViewGPS.setText("!!! GPS отсутствует !!!");
            textViewGPS.setTextColor(getResources().getColor(R.color.colorGPSoff));
        }
    }

    //загрузка из БД в отдельном потоке
    private void loadDataFromBDInThread() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //загружаем данные из БД
                DialogAddData.dbConnector.open();
                final Cursor cursor = DialogAddData.dbConnector.getCursorForBD();
                cursor.moveToFirst();
                int position = 0;
                while (!cursor.isAfterLast()) {
                    //создаем новую карточку учета
                    final AnimalsCard animalsCard = new AnimalsCard();
                    //заполняем
                    animalsCard.setId(cursor.getLong(0));
                    animalsCard.setTypeObject(cursor.getString(1));
                    animalsCard.setObjectCount(cursor.getString(2));
                    animalsCard.setNumberObject(cursor.getInt(3));
                    animalsCard.setYoungObject(cursor.getString(4));
                    animalsCard.setTimeObject(cursor.getString(5));
                    animalsCard.setDateObject(cursor.getString(6));
                    animalsCard.setCoordObjectLat(cursor.getString(7));
                    animalsCard.setCoordObjectLong(cursor.getString(8));
                    animalsCard.setNumPadObject(cursor.getString(9));
                    animalsCard.setNumberYoung(cursor.getInt(10));
                    DialogAddData.animalsCards.add(0, animalsCard);
                    DialogAddData.fillList(DialogAddData.animalsCardAdapter);
                    cursor.moveToNext();
                    position++;
                }
                // make sure to close the cursor
                cursor.close();
                DialogAddData.dbConnector.close();
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void loadDataFromBD() {
        //загружаем данные из БД
        DialogAddData.dbConnector.open();
        DialogAddData.dbConnector.loadData2AnimalsCards(DialogAddData.animalsCards);
        DialogAddData.dbConnector.close();
        DialogAddData.fillList(DialogAddData.animalsCardAdapter);
    }

    private class AsyncLoadDataFromBD extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            outDataButton.setEnabled(false);
            addButton.setEnabled(false);
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            int values = 0;
            //загружаем данные из БД
            DialogAddData.dbConnector.open();
            final Cursor cursor = DialogAddData.dbConnector.getCursorForBD();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                //создаем новую карточку учета
                final AnimalsCard animalsCard = new AnimalsCard();
                //заполняем
                animalsCard.setId(cursor.getLong(0));
                animalsCard.setTypeObject(cursor.getString(1));
                animalsCard.setObjectCount(cursor.getString(2));
                animalsCard.setNumberObject(cursor.getInt(3));
                animalsCard.setYoungObject(cursor.getString(4));
                animalsCard.setTimeObject(cursor.getString(5));
                animalsCard.setDateObject(cursor.getString(6));
                animalsCard.setCoordObjectLat(cursor.getString(7));
                animalsCard.setCoordObjectLong(cursor.getString(8));
                animalsCard.setNumPadObject(cursor.getString(9));
                animalsCard.setNumberYoung(cursor.getInt(10));
                DialogAddData.animalsCards.add(0, animalsCard);
                cursor.moveToNext();
                values++;
                publishProgress(values);
            }
            // make sure to close the cursor
            cursor.close();
            DialogAddData.dbConnector.close();
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            DialogAddData.fillList(DialogAddData.animalsCardAdapter);
            //textViewGPS.setText(Arrays.toString(values));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //textViewGPS.setText(result);
            outDataButton.setEnabled(true);
            addButton.setEnabled(true);
            Toast.makeText(getBaseContext(), getResources().getText(R.string.textLoadData) +
                    Integer.toString(DialogAddData.animalsCards.size()), Toast.LENGTH_SHORT).show();
        }
    }

    /*
    @Override
    protected void onStart() {
        super.onStart();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_TIME_UPDATE, MIN_DISTANCE, locationListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationManager.removeUpdates(locationListener);
    }
    */

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                //обновляем данные координат
                textViewGPS.setText("GPS: " + String.format("%.5f", location.getLatitude()) + ", " +
                        String.format("%.5f", location.getLongitude()) );
                textViewGPS.setTextColor(getResources().getColor(R.color.colorGPS));
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            //if (provider.equals(LocationManager.GPS_PROVIDER)) {
            //    tvStatusGPS.setText("status: " + String.valueOf(status));
            //}
        }

        @Override
        public void onProviderEnabled(String provider) {
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                textViewGPS.setText("GPS: 0.00000, 0.00000");
                textViewGPS.setTextColor(getResources().getColor(R.color.colorGPS));
                return;
            }
            if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
                textViewGPS.setText("GPS: " +   String.format("%.5f", location.getLatitude()) + ", " +
                        String.format("%.5f", location.getLongitude()) );
                textViewGPS.setTextColor(getResources().getColor(R.color.colorGPS));
                }
        }

        @Override
        public void onProviderDisabled(String provider) {
            textViewGPS.setText("!!! GPS OFF !!!");
            textViewGPS.setTextColor(getResources().getColor(R.color.colorGPSoff));
        }
    };

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.listViewAnimals) { // проверяем, что нажали именно на листвью
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int position = info.position; //узнаем, на какой элемент нажали
            //
            menu.setHeaderTitle(DialogAddData.animalsCards.get(position).getTypeObject()); // установим заголовок меню = тексту на который нажали
            super.onCreateContextMenu(menu, v, menuInfo);
            getMenuInflater().inflate(R.menu.contentmenu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info;
        switch (item.getItemId()) {
            case R.id.edit:
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                DialogAddData.currentPosition = info.position; //if edit current position
                if (DialogAddData.dfAddType == null) {
                    DialogAddData.dfAddType = new DialogAddData.DialogAddType();
                }
                DialogAddData.dfAddType.show(getFragmentManager(), "dfAddType");
                break;
            case R.id.delete:
                //узнаем, на какой элемент нажали
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                //удаляем элемент из БД
                DialogAddData.dbConnector.deleteAnimalsCard(DialogAddData.animalsCards.get(info.position).getId());
                //удаляем элемент из списка
                DialogAddData.animalsCards.remove(info.position);
                DialogAddData.fillList(DialogAddData.animalsCardAdapter);
                Toast.makeText(getApplicationContext(), R.string.textDelete, Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addButtton:
                if (TEST == false) {
                    DialogAddData.currentPosition = DialogAddData.NEWPOSITION; //if add new position.
                    if (DialogAddData.dfAddDefaultData == null) {
                        DialogAddData.dfAddDefaultData = new DialogAddData.DialogAddDefaultData();
                    }
                    DialogAddData.dfAddDefaultData.show(getFragmentManager(), "dfAddDefaultData");
                } else {
                    addTestData("11.01.2016");
                    addTestData("13.01.2016");
                }
                break;
            case R.id.outDataButton:
                if (DialogAddData.dfOutData == null) {
                    DialogAddData.dfOutData = new DialogAddData.DialogOutData();
                }
                DialogAddData.dfOutData.show(getFragmentManager(), "dfOutData");
            default:
                break;
        }

    }

    private void addTestData(String date) {
        for (int i = 0; i < 5; i++) {
                    AnimalsCard animalsCard = new AnimalsCard();
                    animalsCard.setNumberYoung(0);
                    animalsCard.setYoungObject("Да");
                    animalsCard.setCoordObjectLat("3.99887");
                    animalsCard.setCoordObjectLong("5.44332");
                    animalsCard.setDateObject(date);
                    animalsCard.setTimeObject("11:12");
            switch (i) {
                case 0:
                    animalsCard.setNumPadObject("3");
                    animalsCard.setNumberObject(i+3);
                    animalsCard.setTypeObject("Заяц");
                    animalsCard.setObjectCount("Следы");
                    break;
                case 1:
                    animalsCard.setNumPadObject("3");
                    animalsCard.setNumberObject(i);
                    animalsCard.setTypeObject("Волк");
                    animalsCard.setObjectCount("Следы");
                    break;
                case 2:
                    animalsCard.setNumPadObject("6");
                    animalsCard.setNumberObject(i);
                    animalsCard.setTypeObject("Лиса");
                    animalsCard.setObjectCount("Следы");
                    break;
                case 3:
                    animalsCard.setNumPadObject("6");
                    animalsCard.setNumberObject(i);
                    animalsCard.setTypeObject("Кабан");
                    animalsCard.setObjectCount("Особь");
                    break;
                case 4:
                    animalsCard.setNumPadObject("6");
                    animalsCard.setNumberObject(i);
                    animalsCard.setTypeObject("Кабан");
                    animalsCard.setObjectCount("Следы");
                    break;
            }

                    DialogAddData.animalsCards.add(0, animalsCard);
                    DialogAddData.fillList(DialogAddData.animalsCardAdapter);
                    //write data to BD
                    DialogAddData.dbConnector.insertAnimalsCard(animalsCard);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.helpmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.help:
                //вызываем диалог добавления объекта учета
                if (DialogAddData.dfHelp == null) {
                    DialogAddData.dfHelp = new DialogAddData.DialogHelp();
                }
                DialogAddData.dfHelp.show(getFragmentManager(), "dfHelp");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}