package ru.allfound.anim2GISpr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * DBConnector.java    v.1.0 28.12.2015
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class DBConnector {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "anim2GISpr";
    private static final String TABLE_ANIMALS = "animals";
    private SQLiteDatabase database;
    private DatabaseOpenHelper databaseOpenHelper;
    private static String tableName;
    private Context context;
    private Handler handler;
    private TextView textViewLinks;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public DBConnector(Context context)
    {
        tableName = TABLE_ANIMALS;
        databaseOpenHelper =
                new DatabaseOpenHelper(context);
        this.context = context;
    }

    public void open() throws SQLException
    {
        database = databaseOpenHelper.getWritableDatabase();
    }

    public void close()
    {
        if (database != null)
            database.close();
    }

    public long insertAnimalsCard(AnimalsCard animalsCard)
    {
        ContentValues newCard = new ContentValues();
        newCard.put("type", animalsCard.getTypeObject());
        newCard.put("object", animalsCard.getObjectCount());
        newCard.put("number", animalsCard.getNumberObject());
        newCard.put("young", animalsCard.getYoungObject());
        newCard.put("time", animalsCard.getTimeObject());
        newCard.put("date", animalsCard.getDateObject());
        newCard.put("latitude", animalsCard.getCoordObjectLat());
        newCard.put("longitude", animalsCard.getCoordObjectLong());
        newCard.put("numpad", animalsCard.getNumPadObject());
        newCard.put("numberyoung", animalsCard.getNumberYoung());
        open();
        long rowID = database.insert(tableName, null, newCard);
        animalsCard.setId(rowID);
        close();
        return rowID;
    }

    // updates an existing contact in the database
    public void updateAnimalsCard(AnimalsCard animalsCard)
    {
        ContentValues editCard = new ContentValues();
        editCard.put("type", animalsCard.getTypeObject());
        editCard.put("object", animalsCard.getObjectCount());
        editCard.put("number", animalsCard.getNumberObject());
        editCard.put("young", animalsCard.getYoungObject());
        editCard.put("time", animalsCard.getTimeObject());
        editCard.put("date", animalsCard.getDateObject());
        editCard.put("latitude", animalsCard.getCoordObjectLat());
        editCard.put("longitude", animalsCard.getCoordObjectLong());
        editCard.put("numpad", animalsCard.getNumPadObject());
        editCard.put("numberyoung", animalsCard.getNumberYoung());
        open(); // open the database
        database.update(tableName, editCard, "_id=" + animalsCard.getId(), null);
        close(); // close the database
    } // end method updateContact

    public Cursor getCursorForBD() {
        return database.query(tableName, null, null, null, null, null, null);
    }

    public void loadData2AnimalsCards(ArrayList<AnimalsCard> animalsCards) {
        Cursor cursor = database.query(tableName, null, null, null, null, null, null);
        cursor.moveToFirst();
        int position = 0;
        while (!cursor.isAfterLast()) {
            //создаем новую карточку учета
            AnimalsCard animalsCard = new AnimalsCard();
            //заполняем
            animalsCard.setId(cursor.getLong(0));
            animalsCard.setTypeObject(cursor.getString(1));
            animalsCard.setObjectCount(cursor.getString(2));
            animalsCard.setNumberObject(cursor.getInt(3));
            animalsCard.setYoungObject(cursor.getString(4));
            animalsCard.setTimeObject(cursor.getString(5));
            animalsCard.setDateObject(cursor.getString(6));
            animalsCard.setCoordObjectLat(cursor.getString(7));
            animalsCard.setCoordObjectLong(cursor.getString(8));
            animalsCard.setNumPadObject(cursor.getString(9));
            animalsCard.setNumberYoung(cursor.getInt(10));
            animalsCards.add(0, animalsCard);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
    }

    // return a Cursor with all contact names in the database
    public Cursor getAllAnimals()
    {
        return database.query(tableName, new String[] {"_id", "name"}, null, null, null, null, "name");
    }

    // return a Cursor containing specified contact's information
    public Cursor getAnimal(long id)
    {
        return database.query(tableName, null, "_id=" + id, null, null, null, null);
    }

    public void deleteAnimalsCard(long id)
    {
        open();
        database.delete(tableName, "_id=" + id, null);
        close();
    }

    public void deleteAllData() {
        open();
        database.delete(tableName, null, null);
        close();
    }

    private class DatabaseOpenHelper extends SQLiteOpenHelper
    {
        public DatabaseOpenHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            String createQuery = "CREATE TABLE " + tableName +
                    "(_id integer primary key autoincrement," +
                    "type TEXT, object TEXT, number INTEGER, " +
                    "young TEXT, time TEXT, date TEXT, " +
                    "latitude TEXT, longitude TEXT, numpad TEXT, numberyoung INTEGER);";

            db.execSQL(createQuery);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion)
        {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL("DROP TABLE IF EXISTS " + tableName);
            onCreate(db);
        }
    }
}
