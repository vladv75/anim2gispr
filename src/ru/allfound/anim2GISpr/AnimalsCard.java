package ru.allfound.anim2GISpr;

/*
 * AnimalsCard.java    v.1.0 05.12.2015
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class AnimalsCard {
    private long id;
    private String typeObject;
    private String objectCount;
    private Integer numberObject;
    private String youngObject;
    private String timeObject;
    private String dateObject;
    private String coordObjectLat;
    private String coordObjectLong;
    private String numPadObject;
    private Integer numberYoung;

    public AnimalsCard() {
    }

    public Integer getNumberYoung() {
        return numberYoung;
    }

    public void setNumberYoung(Integer numberYoung) {
        this.numberYoung = numberYoung;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getObjectCount() {
        return objectCount;
    }

    public void setObjectCount(String objectCount) {
        this.objectCount = objectCount;
    }

    public String getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(String typeObject) {
        this.typeObject = typeObject;
    }

    public Integer getNumberObject() {
        return numberObject;
    }

    public void setNumberObject(Integer numberObject) {
        this.numberObject = numberObject;
    }

    public String getYoungObject() {
        return youngObject;
    }

    public void setYoungObject(String youngObject) {
        this.youngObject = youngObject;
    }

    public String getTimeObject() {
        return timeObject;
    }

    public void setTimeObject(String timeObject) {
        this.timeObject = timeObject;
    }

    public String getDateObject() {
        return dateObject;
    }

    public void setDateObject(String dateObject) {
        this.dateObject = dateObject;
    }

    public String getCoordObjectLat() {
        return coordObjectLat;
    }

    public void setCoordObjectLat(String coordObjectLat) {
        this.coordObjectLat = coordObjectLat;
    }

    public String getCoordObjectLong() {
        return coordObjectLong;
    }

    public void setCoordObjectLong(String coordObjectLong) {
        this.coordObjectLong = coordObjectLong;
    }

    public String getNumPadObject() {
        return numPadObject;
    }

    public void setNumPadObject(String numPadObject) {
        this.numPadObject = numPadObject;
    }
}
