package ru.allfound.anim2GISpr;

import android.content.Context;

import java.util.ArrayList;

/*
 * Data2Reports.java    v.1.0 15.01.2016
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class Data2Reports {

    //данные отчета учета охотничьих животных
    protected class CountingAnimalsReport {
        protected String typeAnimals;
        protected Integer numAnimalsSled;
        protected Integer numAnimalsObject;
        protected Integer allNumAnimals;

        public CountingAnimalsReport() {
            numAnimalsSled = 0;
            numAnimalsObject = 0;
            allNumAnimals = 0;
        }
    }

    //список отчетов по номерам площадок и дням
    protected class CountingReportForPad {
        protected ArrayList<CountingAnimalsReport> countingAnimalsReportLists; //array data
        protected String NupPad;
        protected String Date;
        public CountingReportForPad() {
            countingAnimalsReportLists = new ArrayList<>();
        }
    }

    private Context context;
    public ArrayList<CountingReportForPad> countingReportForPadLists;

    public Data2Reports(Context context) {
        this.context = context;
        countingReportForPadLists = new ArrayList<>();
    }

    public void addData2countingReportForPadLists(AnimalsCard animalsCard) {
        for (CountingReportForPad countingReportForPad : countingReportForPadLists){//проверяем все отчеты
            if (countingReportForPad.NupPad.equals(animalsCard.getNumPadObject()) & //если для площадки с такой же
                    countingReportForPad.Date.equals(animalsCard.getDateObject()) ) //датой уже есть отчет
            {
                addData2CountingAnimalsReportLists(animalsCard, countingReportForPad);
                return;
            }
        }
        //если для площадки отчетов еще нет - добавляем
        CountingReportForPad countingReportForPad = new CountingReportForPad();
        countingReportForPad.Date = animalsCard.getDateObject();
        countingReportForPad.NupPad = animalsCard.getNumPadObject();
        CountingAnimalsReport countingAnimalsReport = new CountingAnimalsReport();
        countingAnimalsReport.typeAnimals = animalsCard.getTypeObject();
        addData2CountingAnimalsReport(animalsCard, countingAnimalsReport);
        countingReportForPad.countingAnimalsReportLists.add(countingAnimalsReport);
        //добавляем новый отчет в список отчетов
        countingReportForPadLists.add(countingReportForPad);
    }

    private void addData2CountingAnimalsReportLists(AnimalsCard animalsCard, CountingReportForPad countingReportForPad) {
        for (CountingAnimalsReport countingAnimalsReport : countingReportForPad.countingAnimalsReportLists) {//проверяем все записи
            if (countingAnimalsReport.typeAnimals.equals(animalsCard.getTypeObject())) {//если есть такая запись
                addData2CountingAnimalsReport(animalsCard, countingAnimalsReport);
                return;
            }
        }
        //создаем новую запись отчета
        CountingAnimalsReport caReport = new CountingAnimalsReport();
        addData2CountingAnimalsReport(animalsCard, caReport);
        caReport.typeAnimals = animalsCard.getTypeObject();
        countingReportForPad.countingAnimalsReportLists.add(caReport);
    }

    private void addData2CountingAnimalsReport(AnimalsCard animalsCard, CountingAnimalsReport countingAnimalsReport) {
        if (animalsCard.getObjectCount().equals(context.getResources().getString(R.string.arrayObjectsSled))) {//если Следы
            countingAnimalsReport.numAnimalsSled += animalsCard.getNumberObject();
        } else { //если животное в натуре
            countingAnimalsReport.numAnimalsObject += animalsCard.getNumberObject();
        }
        //подсчитываем общее количество животных
        countingAnimalsReport.allNumAnimals = countingAnimalsReport.numAnimalsSled +
                countingAnimalsReport.numAnimalsObject;
    }

    public void clearDataReport() {
        countingReportForPadLists.clear();
    }

}
