package ru.allfound.anim2GISpr;

import android.app.DialogFragment;
import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import android.location.Location;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/*
 * DialogAddData.java    v.1.0 23.12.2015
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class DialogAddData {
    public static final int NULL = 0;
    public static final int NEWPOSITION = -1;
    static ArrayList<AnimalsCard> animalsCards;
    static AnimalsCard animalsCard;
    static int currentPosition; //current position in the animalsCards, if ==-1 - new position
    static ListView listViewAnimals;
    static AnimalsCardAdapter animalsCardAdapter;
    private static Context context;
    public static DialogFragment dfAddDefaultData   = null;
    public static DialogFragment dfAddType          = null;
    public static DialogFragment dfAddObject        = null;
    public static DialogFragment dfAddNumberAnimals = null;
    public static DialogFragment dfAddYoungType     = null;
    public static DialogFragment dfAddNumberPad     = null;
    public static DialogFragment dfOutData          = null;
    public static DialogFragment dfHelp             = null;
    public static DialogFragment dfAddNumberYoung   = null;
    public static DBConnector dbConnector;

    public static void fillList(AnimalsCardAdapter animalsCardAdapter) {
        listViewAnimals.setAdapter(animalsCardAdapter);
    }

    public DialogAddData(Context context) {
        DialogAddData.context = context;
        animalsCards = new ArrayList<>();
        currentPosition = NEWPOSITION;
        animalsCardAdapter = new AnimalsCardAdapter(animalsCards, context);
        dbConnector = new DBConnector(context);
    }


    public static class DialogAddDefaultData extends DialogFragment {

        public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddDefaultData);
            View v = inflater.inflate(R.layout.dialogadddefaultdata, null);
            Calendar calendar = Calendar.getInstance();
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            int date = calendar.get(Calendar.DATE);
            int month = calendar.get(Calendar.MONTH) + 1;
            int year = calendar.get(Calendar.YEAR);
            final String textTime;
            if (minute < 10) {
                textTime = hour + ":0" + minute;
            } else {
                textTime = hour + ":" + minute;
            }
            String textDay;
            if (date < 10) {
                textDay = "0" + date;
            } else {
                textDay = "" + date;
            }
            String textMonth;
            if (month < 10) {
                textMonth = "0" + month;
            } else {
                textMonth = "" + month;
            }
            final String textDate = textDay + "." + textMonth + "." + year;
            TextView textView = (TextView) v.findViewById(R.id.textViewTime);
            textView.setText( getResources().getText(R.string.textTime_data) + textTime);
            textView = (TextView) v.findViewById(R.id.textViewDate);
            textView.setText( getResources().getText(R.string.textDate_data) + textDate);
            //узнаем координаты
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            textView = (TextView) v.findViewById(R.id.textViewCoordinate);
            textView.setText(R.string.textCoordinates_data);
            double latitude = NULL;
            double longitude = NULL;
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
            final String latitudeString = String.format(Locale.ENGLISH, "%.5f", latitude);
            final String longitudeString = String.format(Locale.ENGLISH, "%.5f", longitude);
            textView = (TextView) v.findViewById(R.id.textViewLat);
            textView.setText(getResources().getText(R.string.textCoordinateLat) + latitudeString );
            textView = (TextView) v.findViewById(R.id.textViewLong);
            textView.setText(getResources().getText(R.string.textCoordinateLong) + longitudeString );

            v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().cancel();
                }
            });

            v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //создаем новую карточку учета
                    animalsCard = new AnimalsCard();
                    //заполняем
                    animalsCard.setTimeObject(textTime);
                    animalsCard.setDateObject(textDate);
                    animalsCard.setCoordObjectLat(latitudeString);
                    animalsCard.setCoordObjectLong(longitudeString);
                    //закраем текущий диалог
                    getDialog().dismiss();
                    //dfAddDefaultData.onDestroyView();
                    //dfAddDefaultData = null;
                    //вызываем диалог добавления объекта учета
                    if (dfAddType == null) {
                        dfAddType = new DialogAddType();
                    }
                    dfAddType.show(getFragmentManager(), "dfAddType");
                }
            });
            return v;
        }
    }


    public static class DialogAddType extends DialogFragment {
        private ListView listViewObjects;
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddType);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddType.setCancelable(false);
            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayTypeAnimals,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) { //if add new data

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().dismiss();
                        //вызываем диалог добавления объекта учета
                        dfAddDefaultData.show(getFragmentManager(), "dfAddDefaultData");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayTypeAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            animalsCard.setTypeObject(arrayData[itemPosition]);
                            //закраем текущий диалог
                            getDialog().dismiss();
                            //dfAddType.onDestroyView();
                            //dfAddType = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddObject == null) {
                                dfAddObject = new DialogAddObject();
                            }
                            dfAddObject.show(getFragmentManager(), "dfAddObject");
                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } else { //if edit current position
                getDialog().setTitle(animalsCards.get(currentPosition).getTypeObject() + " ->" );
                //button return is disable
                v.findViewById(R.id.btnReturn).setEnabled(false);

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayTypeAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем новым значением
                            animalsCards.get(currentPosition).setTypeObject(arrayData[itemPosition]);
                            //закраем текущий диалог
                            getDialog().dismiss();
                            //dfAddType.onDestroyView();
                            //dfAddType = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddObject == null) {
                                dfAddObject = new DialogAddObject();
                            }
                            dfAddObject.show(getFragmentManager(), "dfAddObject");

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }

    public static class DialogAddObject extends DialogFragment {
        private ListView listViewObjects;

        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddObject);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddObject.setCancelable(false);
            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayObjects,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) { //if add new data
                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddType.show(getFragmentManager(), "dfAddType");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayObjects);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            animalsCard.setObjectCount(arrayData[itemPosition]);

                            //закрываем текущий диалог
                            getDialog().dismiss();
                            //dfAddObject.onDestroyView();
                            //dfAddObject = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddNumberAnimals == null) {
                                dfAddNumberAnimals = new DialogAddNumberAnimals();
                            }
                            dfAddNumberAnimals.show(getFragmentManager(), "dfAddNumberAnimals");

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                getDialog().setTitle(getResources().getText(R.string.textObject_data) +
                        animalsCards.get(currentPosition).getObjectCount() + " ->" );

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddType.show(getFragmentManager(), "dfAddType");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayObjects);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            //animalsCard.setObjectCount(arrayData[itemPosition]);
                            animalsCards.get(currentPosition).setObjectCount(arrayData[itemPosition]);

                            //закрываем текущий диалог
                            getDialog().dismiss();
                            //dfAddObject.onDestroyView();
                            //dfAddObject = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddNumberAnimals == null) {
                                dfAddNumberAnimals = new DialogAddNumberAnimals();
                            }
                            dfAddNumberAnimals.show(getFragmentManager(), "dfAddNumberAnimals");

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }



    public static class DialogAddNumberAnimals extends DialogFragment {
        static ListView listViewObjects;
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddNumberAnimals);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddNumberAnimals.setCancelable(false);

            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayNumberAnimals,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) {
                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddObject.show(getFragmentManager(), "dfAddObject");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            animalsCard.setNumberObject(itemPosition+1);
                            //закрываем текущий диалог
                            getDialog().dismiss();
                            //dfAddNumberAnimals.onDestroyView();
                            //dfAddNumberAnimals = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddYoungType == null) {
                                dfAddYoungType = new DialogAddYoungType();
                            }
                            dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                getDialog().setTitle(getResources().getText(R.string.textQuantity_data) +
                        animalsCards.get(currentPosition).getNumberObject().toString() + " ->");

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddObject.show(getFragmentManager(), "dfAddObject");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            //animalsCard.setNumberObject(arrayData[itemPosition]);
                            animalsCards.get(currentPosition).setNumberObject(itemPosition+1);
                            //закрываем текущий диалог
                            getDialog().dismiss();
                            //dfAddNumberAnimals.onDestroyView();
                            //dfAddNumberAnimals = null;
                            //вызываем диалог добавления объекта учета
                            if (dfAddYoungType == null) {
                                dfAddYoungType = new DialogAddYoungType();
                            }
                            dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }


    public static class DialogAddYoungType extends DialogFragment {
        private ListView listViewObjects;
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddOYoungType);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddYoungType.setCancelable(false);
            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayYoungType,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) {
                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddNumberAnimals.show(getFragmentManager(), "dfAddNumberAnimals");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayYoungType);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            animalsCard.setYoungObject(arrayData[itemPosition]);
                            //закрываем текущий диалог
                            getDialog().dismiss();
                            //dfAddYoungType.onDestroyView();
                            //dfAddYoungType = null;
                            //вызываем диалог добавления объекта учета
                            if (arrayData[itemPosition].equals(arrayData[0])) { //если выбранная позиция Да
                                //вызываем диалог ввода количества молодняка
                                if (dfAddNumberYoung == null) {
                                    dfAddNumberYoung = new DialogAddNumberYoung();
                                }
                                dfAddNumberYoung.show(getFragmentManager(), "dfAddNumberYoung");
                            } else { //если Нет то переход на диалог кол-ва площадок
                                animalsCard.setNumberYoung(0);
                                //вызываем диалог добавления объекта учета
                                if (dfAddNumberPad == null) {
                                    dfAddNumberPad = new DialogAddNumberPad();
                                }
                                dfAddNumberPad.show(getFragmentManager(), "dfAddNumberPad");
                            }

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                getDialog().setTitle(getResources().getText(R.string.textYoungAnimals_data) +
                        animalsCards.get(currentPosition).getYoungObject() + " ->");

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddNumberAnimals.show(getFragmentManager(), "dfAddNumberAnimals");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayYoungType);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //заполняем
                            animalsCards.get(currentPosition).setYoungObject(arrayData[itemPosition]);
                            //закрываем текущий диалог
                            getDialog().dismiss();

                            if (arrayData[itemPosition].equals(arrayData[0])) { //если выбранная позиция Да
                                //вызываем диалог ввода количества молодняка
                                if (dfAddNumberYoung == null) {
                                    dfAddNumberYoung = new DialogAddNumberYoung();
                                }
                                dfAddNumberYoung.show(getFragmentManager(), "dfAddNumberYoung");
                            } else { //если Нет то переход на диалог кол-ва площадок
                                //вызываем диалог добавления объекта учета
                                if (dfAddNumberPad == null) {
                                    dfAddNumberPad = new DialogAddNumberPad();
                                }
                                dfAddNumberPad.show(getFragmentManager(), "dfAddNumberPad");
                            }

                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }


    public static class DialogAddNumberYoung extends DialogFragment {
        private ListView listViewObjects;
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddNumberYoung);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddNumberYoung.setCancelable(false);
            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayNumberYoung,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) {
                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            if (itemPosition <= DialogAddNumberAnimals.listViewObjects.getCheckedItemPosition()) {
                                //заполняем
                                animalsCard.setNumberYoung(itemPosition+1);
                                //закрываем текущий диалог
                                getDialog().dismiss();
                                //вызываем диалог добавления объекта учета
                                if (dfAddNumberPad == null) {
                                    dfAddNumberPad = new DialogAddNumberPad();
                                }
                                dfAddNumberPad.show(getFragmentManager(), "dfAddNumberPad");
                            } else {
                                Toast.makeText(context, R.string.TextNotCorrectData, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                getDialog().setTitle(getResources().getText(R.string.textQuantity_data) +
                        animalsCards.get(currentPosition).getNumberYoung().toString() + " ->");

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberAnimals);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            if (itemPosition <= DialogAddNumberAnimals.listViewObjects.getCheckedItemPosition()) {
                                //заполняем
                                //animalsCard.setNumberObject(arrayData[itemPosition]);
                                animalsCards.get(currentPosition).setNumberYoung(itemPosition+1);
                                //закрываем текущий диалог
                                getDialog().dismiss();
                                //вызываем диалог добавления объекта учета
                                //вызываем диалог добавления объекта учета
                                if (dfAddNumberPad == null) {
                                    dfAddNumberPad = new DialogAddNumberPad();
                                }
                                dfAddNumberPad.show(getFragmentManager(), "dfAddNumberPad");
                            } else {
                                Toast.makeText(context, R.string.TextNotCorrectData, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }

    public static class DialogAddNumberPad extends DialogFragment {
        private ListView listViewObjects;
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogAddNumberPad);
            View v = inflater.inflate(R.layout.dialogaddata, null);
            dfAddNumberPad.setCancelable(false);
            listViewObjects = (ListView) v.findViewById(R.id.listViewObjects);
            // устанавливаем режим выбора пунктов списка
            listViewObjects.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Создаем адаптер, используя массив из файла ресурсов
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource( context, R.array.arrayNumberPad,
                    android.R.layout.simple_list_item_single_choice);
            listViewObjects.setAdapter(adapter);
            if (currentPosition < 0) {
                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberAnimals);
                        if (animalsCard.getYoungObject().equals(arrayData[0])) { //если выбранная позиция Да
                            //вызываем диалог ввода количества молодняка
                            if (dfAddNumberYoung == null) {
                                dfAddNumberYoung = new DialogAddNumberYoung();
                            }
                            dfAddNumberYoung.show(getFragmentManager(), "dfAddNumberYoung");
                        } else { //если Нет то переход на диалог есть ли молодняк
                            //вызываем диалог добавления объекта учета
                            if (dfAddYoungType == null) {
                                dfAddYoungType = new DialogAddYoungType();
                            }
                            dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");
                        }
                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberPad);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //add and fill card
                            animalsCard.setNumPadObject(arrayData[itemPosition]);
                            animalsCards.add(0, animalsCard);
                            fillList(animalsCardAdapter);
                            //write data to BD
                            dbConnector.insertAnimalsCard(animalsCard);

                            //закрываем текущий диалог
                            dfAddNumberPad.onDestroyView();
                            dfAddNumberPad = null;

                            dfAddDefaultData.onDestroyView();
                            dfAddDefaultData   = null;
                            dfAddType.onDestroyView();
                            dfAddType          = null;
                            dfAddObject.onDestroyView();
                            dfAddObject        = null;
                            dfAddNumberAnimals.onDestroyView();
                            dfAddNumberAnimals = null;
                            dfAddYoungType.onDestroyView();
                            dfAddYoungType     = null;
                            if (dfAddNumberYoung != null) {
                                dfAddNumberYoung.onDestroyView();
                                dfAddNumberYoung = null;
                            }
                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                getDialog().setTitle(getResources().getText(R.string.textPad_data) +
                        animalsCards.get(currentPosition).getNumPadObject() + " ->");

                v.findViewById(R.id.btnReturn).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getDialog().dismiss();

                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberYoung);
                        if (animalsCard.getYoungObject().equals(arrayData[0])) { //если выбранная позиция Да
                            //вызываем диалог ввода количества молодняка
                            if (dfAddNumberYoung == null) {
                                dfAddNumberYoung = new DialogAddNumberYoung();
                            }
                            dfAddNumberYoung.show(getFragmentManager(), "dfAddNumberYoung");
                        } else { //если Нет то переход на диалог есть ли молодняк
                            //вызываем диалог добавления объекта учета
                            if (dfAddYoungType == null) {
                                dfAddYoungType = new DialogAddYoungType();
                            }
                            dfAddYoungType.show(getFragmentManager(), "dfAddYoungType");
                        }

                    }
                });

                v.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animalsCard = null;
                        getDialog().cancel();
                    }
                });

                v.findViewById(R.id.btnFolow).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // получаем массив из файла ресурсов
                        String[] arrayData;
                        arrayData = getResources().getStringArray(R.array.arrayNumberPad);
                        //находим выбранную позицию
                        int itemPosition = listViewObjects.getCheckedItemPosition();
                        if (itemPosition >= NULL) {
                            //add and fill card
                            animalsCards.get(currentPosition).setNumPadObject(arrayData[itemPosition]);
                            fillList(animalsCardAdapter);
                            //update data from BD
                            dbConnector.updateAnimalsCard(animalsCards.get(currentPosition));

                            //закрываем текущий диалог
                            dfAddNumberPad.onDestroyView();
                            dfAddNumberPad = null;

                            dfAddType.onDestroyView();
                            dfAddType          = null;
                            dfAddObject.onDestroyView();
                            dfAddObject        = null;
                            dfAddNumberAnimals.onDestroyView();
                            dfAddNumberAnimals = null;
                            dfAddYoungType.onDestroyView();
                            dfAddYoungType     = null;
                            if (dfAddNumberYoung != null) {
                                dfAddNumberYoung.onDestroyView();
                                dfAddNumberYoung = null;
                            }
                        } else {
                            Toast.makeText(context, R.string.TextNotSetData, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            return v;
        }
    }


    //диалог выгрузки и очистки данных
    public static class DialogOutData extends DialogFragment {
        private Data2File data2File;
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle saveInstanceState) {

            getDialog().setTitle(R.string.textTitleDialogOutData);
            View v = inflater.inflate(R.layout.dialogoutdata, null);

            v.findViewById(R.id.buttonData2Report).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data2File = new Data2File();
                    data2File.writeCountingReport2SD(DialogAddData.animalsCards, context);
                    data2File = null;
                }
            });

            v.findViewById(R.id.buttonData2CSV).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data2File = new Data2File();
                    data2File.writeFile2SD(DialogAddData.animalsCards, context);
                    data2File = null;
                }
            });

            v.findViewById(R.id.buttonOutClearData).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data2File = new Data2File();
                    data2File.writeFile2SD(DialogAddData.animalsCards, context);
                    data2File = null;
                    //проверяем есть ли данные для выгрузки
                    if (animalsCards.size() > 0) {
                        animalsCards.clear();
                        fillList(animalsCardAdapter);
                        //delete BD
                        dbConnector.deleteAllData();
                        Toast.makeText(context, R.string.textDataClear, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            v.findViewById(R.id.buttonClearData).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //проверяем есть ли данные для выгрузки
                    if (animalsCards.size() > 0) {
                        animalsCards.clear();
                        fillList(animalsCardAdapter);
                        //delete BD
                        dbConnector.deleteAllData();
                        Toast.makeText(context, R.string.textDataClear, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.textNotData, Toast.LENGTH_SHORT).show();
                    }

                }
            });

            v.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //закрываем текущий диалог
                    dfOutData.onDestroyView();
                    dfOutData = null;
                }
            });

            return v;
        }
    }

    public static class DialogHelp extends DialogFragment {
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle saveInstanceState) {
            getDialog().setTitle(R.string.textTitleDialogHelp);
            View v = inflater.inflate(R.layout.dialoghelp, null);
            TextView tvTextHelp = (TextView) v.findViewById(R.id.textViewHelp);
            String[] array = getResources().getStringArray(R.array.arrayTextHelp);
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < array.length; i++){
                sBuilder.append(array[i]);
                sBuilder.append("\n");
            }
            tvTextHelp.setText(sBuilder.toString());

            v.findViewById(R.id.buttonCancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //закрываем текущий диалог
                    dfHelp.onDestroyView();
                    dfHelp = null;
                }
            });

            return  v;
        }
    }
}
