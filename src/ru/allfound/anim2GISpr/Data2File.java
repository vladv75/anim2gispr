package ru.allfound.anim2GISpr;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/*
 * Data2File.java    v.1.0 08.01.2016
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class Data2File {
    final String DIR_SD = "anim2GISpr_data";
    final String FILENAME_SD = "anim2GISpr";

    public Data2File() {
    }

    void writeFile2SD(ArrayList<AnimalsCard> animalsCards, Context context) {
        //проверяем есть ли данные для выгрузки
        if (animalsCards.size() == 0) {
            Toast.makeText(context, R.string.textNotData, Toast.LENGTH_SHORT).show();
            return;
        }
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(context, context.getResources().getString(R.string.textSDNotAvailable) + Environment.getExternalStorageState(), Toast.LENGTH_SHORT).show();
            return;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // создаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        String filename = FILENAME_SD + "_";
        Calendar calendar = Calendar.getInstance();
        filename = filename + Integer.toString(calendar.get(Calendar.YEAR));
        filename = filename + Integer.toString(calendar.get(Calendar.MONTH));
        filename = filename + Integer.toString(calendar.get(Calendar.DATE));
        filename = filename + Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
        filename = filename + Integer.toString(calendar.get(Calendar.MINUTE));
        filename = filename + Integer.toString(calendar.get(Calendar.SECOND));
        filename = filename + ".csv";
        File sdFile = new File(sdPath, filename);
        try {
            // открываем поток для записи
            BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
            // пишем данные
            //StringBuilder stringBuilder = new StringBuilder();
            String outData;
            //формируем шапку таблицы
            outData = context.getResources().getString(R.string.headerReport);
            bw.write(outData);
            for (AnimalsCard animalsCard : animalsCards) {
                outData = animalsCard.getDateObject() + ",";
                outData = outData + animalsCard.getTimeObject() + ",";
                outData = outData + animalsCard.getTypeObject() + ",";
                outData = outData + animalsCard.getObjectCount() + ",";
                outData = outData + animalsCard.getNumberObject() + ",";
                outData = outData + animalsCard.getYoungObject() + ",";
                outData = outData + animalsCard.getNumberYoung() + ",";
                outData = outData + animalsCard.getCoordObjectLat() + ",";
                outData = outData + animalsCard.getCoordObjectLong() + ",";
                outData = outData + animalsCard.getNumPadObject() + " \n";
                bw.write(outData);
            }
            // закрываем поток
            bw.close();
            //Log.d(LOG_TAG, "Файл записан на SD: " + sdFile.getAbsolutePath());
            Toast.makeText(context, context.getResources().getText(R.string.textDataWrite2SD) +
                    sdFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void writeCountingReport2SD(ArrayList<AnimalsCard> animalsCards, Context context) {
        //проверяем есть ли данные для выгрузки
        if (animalsCards.size() == 0) {
            Toast.makeText(context, R.string.textNotData, Toast.LENGTH_SHORT).show();
            return;
        }
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            //Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            Toast.makeText(context, context.getResources().getText(R.string.textSDNotAvailable) +
                    Environment.getExternalStorageState(), Toast.LENGTH_SHORT).show();
            return;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // создаем каталог
        sdPath.mkdirs();
        //формируем отчет
        Data2Reports data2Reports = new Data2Reports(context);
        for (AnimalsCard animalsCard : animalsCards) {
            data2Reports.addData2countingReportForPadLists(animalsCard);
        }
        boolean reportsCreate = false;
        //выгружаем в файл каждый отчет отдельно
        for (Data2Reports.CountingReportForPad countingReportForPad : data2Reports.countingReportForPadLists) {
            // формируем объект File, который содержит путь к файлу
            String filename = FILENAME_SD + "_";
            filename += countingReportForPad.NupPad + "_";
            filename += countingReportForPad.Date;
            filename = filename + ".csv";
            File sdFile = new File(sdPath, filename);
            try {
                // открываем поток для записи
                BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
                // пишем данные
                String outData;
                //формируем шапку таблицы
                outData = context.getResources().getString(R.string.headerCountingReport);
                bw.write(outData);
                reportsCreate = false;
                for (Data2Reports.CountingAnimalsReport countingAnimalsReport :
                        countingReportForPad.countingAnimalsReportLists)
                {
                    outData = countingAnimalsReport.typeAnimals + ",";
                    outData += countingAnimalsReport.numAnimalsSled + ",";
                    outData += countingAnimalsReport.numAnimalsObject + ",";
                    outData += countingAnimalsReport.allNumAnimals + " \n";
                    bw.write(outData);
                }
                // закрываем поток
                bw.close();
                //Log.d(LOG_TAG, "Файл записан на SD: " + sdFile.getAbsolutePath());
                reportsCreate = true;
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context, R.string.textErrorWrite2SD, Toast.LENGTH_SHORT).show();
                reportsCreate = false;
            }
        }
        if (reportsCreate) {
            Toast.makeText(context, R.string.textReportsWrite2SD, Toast.LENGTH_SHORT).show();
        }
    }
}
