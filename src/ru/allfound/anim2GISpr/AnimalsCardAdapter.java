package ru.allfound.anim2GISpr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/*
 * AnimalsCardAdapter.java    v.1.0 05.12.2015
 *
 * Copyright (c) 2015-2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class AnimalsCardAdapter extends BaseAdapter{
    private ArrayList<AnimalsCard> animalsCards;
    private Context context;

    public AnimalsCardAdapter(ArrayList<AnimalsCard> animalsCards, Context context) {
        this.animalsCards = animalsCards;
        this.context = context;
    }

    @Override
    public int getCount() {
        return animalsCards.size();
    }

    @Override
    public Object getItem(int position) {
        return animalsCards.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AnimalsCard p = animalsCards.get(position);
        View animalsCardView = convertView;
        if (animalsCardView == null) {
            animalsCardView = LayoutInflater.from(context).inflate(R.layout.listitemanimalscards, null);
        }
        TextView textView = (TextView) animalsCardView.findViewById(R.id.textViewType);
        textView.setText(p.getTypeObject());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewObject);
        textView.setText(p.getObjectCount());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewNumber);
        textView.setText(p.getNumberObject().toString());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewTime);
        textView.setText(p.getTimeObject());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewDate);
        textView.setText(p.getDateObject());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewCoordinateLat);
        textView.setText(p.getCoordObjectLat());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewCoordinateLong);
        textView.setText(p.getCoordObjectLong());
        textView = (TextView) animalsCardView.findViewById(R.id.textViewNumberPad);
        textView.setText(p.getNumPadObject());
        // получаем массив из файла ресурсов
        String[] arrayData;
        arrayData = context.getResources().getStringArray(R.array.arrayYoungType);
        if (p.getYoungObject().equals(arrayData[0])) { //если есть молодняк, то выводим количество
            textView = (TextView) animalsCardView.findViewById(R.id.textViewYoung);
            textView.setText(p.getNumberYoung().toString());
        } else {
            textView = (TextView) animalsCardView.findViewById(R.id.textViewYoung);
            textView.setText(p.getYoungObject().toString());
        }
        return animalsCardView;
    }
}
